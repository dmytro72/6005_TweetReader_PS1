package twitter;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

/*
 * Testing strategy for isDistinctTweets
 * 
 * - Number of tweets : 0, 1, more then 1.
 * - equal id : all id equal, some id equal, all id distinct.
 * - Place : equal id adjacent, equal id not adjacent.
 */
/*
 * Testing strategy for isValidUsername
 * Name length : 1 character, more then 1 character
 * Sameness : all symbol same, all symbol different, some symbol same
 * Case sensitivity : lower case letters, upper case letters
 * Valid symbols: all symbol valid, some symbol valid, all symbol not valid
 */
 /* 
 * Testing strategy for isUsernameCharacter
 * - Valid characters : letter, digit, hyphen, underscore.
 * - Some invalid characters. 
 */

public class AssertsAndHelpersTest {

    private static Instant d1;
    private static Instant d2;
    private static Instant d3;
    private static Instant d4;
    
    private static Tweet tweet1;
    private static Tweet tweet2;
    private static Tweet tweet3;
    private static Tweet tweet4;
    private static Tweet tweet5;
    
    
    @BeforeClass
    public static void setUpBeforeClass() {
        d1 = Instant.parse("2015-02-18T10:00:00Z");
        d2 = Instant.parse("2015-02-18T11:00:00Z");
        d3 = Instant.parse("2015-02-18T12:00:00Z");
        d4 = Instant.parse("2015-02-18T13:00:00Z");
        
        tweet1 = new Tweet(0, "alyssa", "is it reasonable to talk about rivest so much?", d1);
        tweet2 = new Tweet(1, "bbitdiddle", "rivest talk in 30 minutes #hype", d2);
        tweet3 = new Tweet(2, "alyssa", "is it reasonable to talk about rivest so much?", d3);
        tweet4 = new Tweet(0, "bbitdiddle", "rivest talk in 30 minutes #hype", d4);
        tweet5 = new Tweet(0, "bbitdiddle", "rivest talk in 30 minutes #hype", d1);
    }
    
    @Test
    public void testIsDistinctTweetsNotModified() {
        
        List<Tweet> tweetsList = Arrays.asList(tweet1, tweet3, tweet4, tweet2);
        Tweet[] listBefore = new Tweet[tweetsList.size()];
        listBefore = tweetsList.toArray(listBefore);
        AssertsAndHelpers.isDistinctTweets(tweetsList);
        Tweet[] listAfter = new Tweet[tweetsList.size()];
        listAfter = tweetsList.toArray(listAfter);
        
        assertArrayEquals(listBefore, listAfter);
    }
    
    @Test
    public void testIsDistinctTweetsEmptyList() {
        
        boolean distinct = AssertsAndHelpers.isDistinctTweets(new ArrayList<Tweet>());
        
        assertTrue(distinct);
    }
    
    @Test
    public void testIsDistinctTweetsOneTweet() {
        
        boolean distinct = AssertsAndHelpers.isDistinctTweets(Arrays.asList(tweet1));
        
        assertTrue(distinct);
    }
    
    @Test
    public void testIsDistinctTweetsAllEqual() {
        
        boolean distinct = AssertsAndHelpers.isDistinctTweets(Arrays.asList(tweet1, tweet4, tweet5));
        
        assertFalse(distinct);
    }
    
    @Test
    public void testIsDistinctTweetsSomeEqual() {
        
        boolean distinct = AssertsAndHelpers.isDistinctTweets(Arrays.asList(tweet1, tweet2, tweet4, tweet3));
        
        assertFalse(distinct);
    }
    
    @Test
    public void testIsDistinctTweetsAllDistinct() {
        
        boolean distinct = AssertsAndHelpers.isDistinctTweets(Arrays.asList(tweet2, tweet1, tweet3));
        
        assertTrue(distinct);
    }
    
    @Test
    public void testIsDistinctTweetsAdjacentEqual() {
        
        boolean distinct = AssertsAndHelpers.isDistinctTweets(Arrays.asList(tweet3, tweet1, tweet4, tweet2));
        
        assertFalse(distinct);
    }@Test
    public void testIsValidUsernameOneValidSymbol() {
        boolean validName = AssertsAndHelpers.isValidUsername("n");
        
        assertTrue(validName);
    }
    
    @Test
    public void testIsValidUsernameOneInvalidSymbol() {
        boolean validName = AssertsAndHelpers.isValidUsername("*");
        
        assertFalse(validName);
    }
    
    @Test
    public void testIsValidUsernameAllSymbolsValid() {
        boolean validName = AssertsAndHelpers.isValidUsername("sPeeRo_21-lEE");
        
        assertTrue(validName);
    }
    
    @Test
    public void testIsValidUsernameSomeSymbolsInvalid() {
        boolean validName = AssertsAndHelpers.isValidUsername("Sam#in$@");
        
        assertFalse(validName);
    }
    
    @Test
    public void testIsValidUsernameAllSymbolsInvalid() {
        boolean validName = AssertsAndHelpers.isValidUsername("*^&&%$#@");
        
        assertFalse(validName);
    }
    
    @Test
    public void testIsValidUsernameAllSymbolsSame() {
        boolean validName = AssertsAndHelpers.isValidUsername("DDDDDD");
        
        assertTrue(validName);
    }
    
    @Test
    public void testIsUsernameCharacterLetter() {
        
        boolean lowercase = AssertsAndHelpers.isUsernameCharacter('i');
        boolean uppercase = AssertsAndHelpers.isUsernameCharacter('M');
        
        assertTrue(lowercase);
        assertTrue(uppercase);
    }
    
    @Test
    public void testIsUsernameCharacterDigit() {
        
        boolean digit = AssertsAndHelpers.isUsernameCharacter('3');
        
        assertTrue(digit);
    }
    
    @Test
    public void testIsUsernameCharacterHyphen() {
        
        boolean hyphen = AssertsAndHelpers.isUsernameCharacter('-');
        
        assertTrue(hyphen);
    }
    
    @Test
    public void testIsUsernameCharacterUnderscore() {
        
        boolean underscore = AssertsAndHelpers.isUsernameCharacter('_');
        
        assertTrue(underscore);
    }
    
    @Test
    public void testIsUsernameCharacterNotValid() {
        
        boolean dot = AssertsAndHelpers.isUsernameCharacter('.');
        boolean comma = AssertsAndHelpers.isUsernameCharacter(',');
        boolean star = AssertsAndHelpers.isUsernameCharacter('*');
        
        assertFalse(dot);
        assertFalse(comma);
        assertFalse(star);
    }@Test
    public void testIsValidWordNoSympols() {
        boolean isValid = AssertsAndHelpers.isValidWord("");
        
        assertFalse(isValid);
    }
    
    @Test
    public void testIsValidWordNoSpace() {
        boolean isValid = AssertsAndHelpers.isValidWord("smyle");
        
        assertTrue(isValid);
    }
    
    @Test
    public void testIsValidWordSpaceAtStart() {
        boolean isValid = AssertsAndHelpers.isValidWord(" is");
        
        assertFalse(isValid);
    }
    
    @Test
    public void testIsValidWordSpaceAtEnd() {
        boolean isValid = AssertsAndHelpers.isValidWord("mister ");
        
        assertFalse(isValid);
    }
    
    @Test
    public void testIsValidWordsSpaceAtMiddle() {
        boolean isValid = AssertsAndHelpers.isValidWord("gra ss");
        
        assertFalse(isValid);
    }
    
    @Test
    public void testIsValidWordsMultipleSpaces() {
        boolean isValid = AssertsAndHelpers.isValidWord("imp leme nted");
        
        assertFalse(isValid);
    }
    
    /* Testing strategy for createMap
     * Number of keys : 0, 1, more then 1;
     */
    @Test
    public void testCreateMapEmptyKey() {
        Map<String, String> testMap = AssertsAndHelpers.createMap(new ArrayList<String>(), new ArrayList<String>());
        
        assertTrue(testMap.keySet().isEmpty());
    }
    
    @Test
    public void testCreateMapOneKey() {
        Map<String, String> testMap = AssertsAndHelpers.createMap(Arrays.asList("greg"), Arrays.asList("my friend"));
        
        assertEquals(1, testMap.keySet().size());
        assertEquals("my friend", testMap.get("greg"));
    }
    
    @Test
    public void testCreateMapMultipleKeys() {
        Map<String, String> testMap = AssertsAndHelpers.createMap(Arrays.asList("ben", "alyssa", "victor", "paul"),
                Arrays.asList("he is classmate", "not remember", "funny boy", "my brother"));
        
        assertEquals(4, testMap.keySet().size());
        assertEquals("he is classmate", testMap.get("ben"));
        assertEquals("not remember", testMap.get("alyssa"));
        assertEquals("funny boy", testMap.get("victor"));
        assertEquals("my brother", testMap.get("paul"));
    }
}
