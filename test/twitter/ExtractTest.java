package twitter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertArrayEquals;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

/*
 * Testing strategy for getTimespan
 * 
 * - Number tweets in list of tweets : 0, 1, more then 1.
 * - Sameness : all tweets time same, part of tweets time same, all tweets time different.
 * - Order : time in nondecreasing order, time in nonincreasing order, time in random order.
 * - Boundaries : several tweets with equal time at the beginning of the List,
 *                several tweets with equal time at the end of the list.
 * - List not modified
 * 
 * 
 * Testing strategy for getMentionedUsers
 * 
 * - Number tweets in list of tweets : 0, 1, more then 1.
 * - Number username-mention : 0, 1, more then 1.
 * - Sameness : all names equal, some names equal, all names different.
 * - Place : name at beginning of tweet, name at the end of tweet, name in the middle.
 * - Case sensitivity : name in upper case, name in lower case, name in different case
 * - Names in tweets : names in all tweets, no names, names in some tweets
 * - List not modified
 */

public class ExtractTest {
    
    private static Instant d1;
    private static Instant d2;
    private static Instant d3;
    private static Instant d4;
    
    private static Tweet tweet1;
    private static Tweet tweet2;
    private static Tweet tweet3;
    private static Tweet tweet4;
    private static Tweet tweet5;
    private static Tweet tweet6;
    private static Tweet tweet7;
    
    @BeforeClass
    public static void setUpBeforeClass() {
        d1 = Instant.parse("2015-02-18T10:00:00Z");
        d2 = Instant.parse("2015-02-18T11:00:00Z");
        d3 = Instant.parse("2015-02-18T12:00:00Z");
        d4 = Instant.parse("2015-02-18T13:00:00Z");
        
        tweet1 = new Tweet(0, "alyssa", "is it reasonable to talk about rivest so much?", d1);
        tweet2 = new Tweet(1, "bbitdiddle", "rivest talk in 30 minutes #hype", d2);
        tweet3 = new Tweet(2, "talor", "@glover tell me about @GREG", d3);
        tweet4 = new Tweet(3, "glover", "I see @tAlOr and @talor looks at me", d4);
        tweet5 = new Tweet(4, "glover", "@talor's my friend", d1);
        tweet6 = new Tweet(5, "alyssa", "my email is alyssa.cabbot@gmail.com", d1);
        tweet7 = new Tweet(6, "bbitdiddle", "i can @ and some - 42<@vezel @", d3);
    }
    
    @Test
    public void testGetTimespanListNotModified() {
        
        List<Tweet> tweetsList = Arrays.asList(tweet1, tweet3, tweet4, tweet2, tweet5);
        Tweet[] listBefore = new Tweet[tweetsList.size()];
        listBefore = tweetsList.toArray(listBefore);
        Extract.getTimespan(tweetsList);
        Tweet[] listAfter = new Tweet[tweetsList.size()];
        listAfter = tweetsList.toArray(listAfter);
        
        assertArrayEquals(listBefore, listAfter);
    }
    
    @Test
    public void testGetTimespanTwoTweets() {
        
        Timespan timespan = Extract.getTimespan(Arrays.asList(tweet1, tweet2));
        
        assertEquals(d1, timespan.getStart());
        assertEquals(d2, timespan.getEnd());
    }
    
    @Test
    public void testGetTimespanEmptyList() {
        Timespan timespan = Extract.getTimespan(new ArrayList<Tweet>());
        
        assertEquals(timespan.getStart(), timespan.getEnd());
    }
    
    @Test
    public void testGetTimespanOneTweet() {
        Timespan timespan = Extract.getTimespan(Arrays.asList(tweet1));
        
        assertEquals(d1, timespan.getStart());
        assertEquals(d1, timespan.getEnd());
    }
    
    @Test
    public void testGetTimespanAllTimeSame() {
        Timespan timespan = Extract.getTimespan(Arrays.asList(tweet1, tweet5, tweet6));
        
        assertEquals(d1, timespan.getStart());
        assertEquals(d1, timespan.getEnd());
    }
    
    @Test
    public void testGetTimespanTimeSameAtStart() {
        Timespan timespan = Extract.getTimespan(Arrays.asList(tweet3, tweet7, tweet4, tweet2));
        
        assertEquals(d2, timespan.getStart());
        assertEquals(d4, timespan.getEnd());
    }
    
    @Test
    public void testGetTimespanTimeSameAtEnd() {
        Timespan timespan = Extract.getTimespan(Arrays.asList(tweet2, tweet4, tweet1, tweet5));
        
        assertEquals(d1, timespan.getStart());
        assertEquals(d4, timespan.getEnd());
    }
    
    @Test
    public void testGetTimespanTimeIncrease() {
        Timespan timespan = Extract.getTimespan(Arrays.asList(tweet1, tweet2, tweet3, tweet4));
        
        assertEquals(d1, timespan.getStart());
        assertEquals(d4, timespan.getEnd());
    }
    
    @Test
    public void testGetTimespanTimeDecrease() {
        Timespan timespan = Extract.getTimespan(Arrays.asList(tweet4, tweet3, tweet2, tweet1));
        
        assertEquals(d1, timespan.getStart());
        assertEquals(d4, timespan.getEnd());
    }
    
    @Test
    public void testGetTimespanTimeRandom() {
        Timespan timespan = Extract.getTimespan(Arrays.asList(tweet3, tweet1, tweet4, tweet2));
        
        assertEquals(d1, timespan.getStart());
        assertEquals(d4, timespan.getEnd());
    }
    
    @Test
    public void testGetMentionedUsersNotModified() {
        
        List<Tweet> tweetsList = Arrays.asList(tweet1, tweet3, tweet4, tweet2, tweet5);
        Tweet[] listBefore = new Tweet[tweetsList.size()];
        listBefore = tweetsList.toArray(listBefore);
        Extract.getMentionedUsers(tweetsList);
        Tweet[] listAfter = new Tweet[tweetsList.size()];
        listAfter = tweetsList.toArray(listAfter);
        
        assertArrayEquals(listBefore, listAfter);
    }
    
    @Test
    public void testGetMentionedUsersEmptyList() {
        Set<String> mentionedUsers = Extract.getMentionedUsers(new ArrayList<Tweet>());
        
        assertTrue(mentionedUsers.isEmpty());
    }
    
    @Test
    public void testGetMentionedUsersNoMention() {
        Set<String> mentionedUsers = Extract.getMentionedUsers(Arrays.asList(tweet1));
    
        assertTrue(mentionedUsers.isEmpty());
    }
    
    @Test
    public void testGetMentionedUsersAllSameAndCaseSensitivity() {
        Set<String> mentionedUsers = Extract.getMentionedUsers(Arrays.asList(tweet1, tweet4, tweet5));
        List<String> mentionedUsersLowercase = new ArrayList<>();
        for (String user : mentionedUsers) {
            mentionedUsersLowercase.add(user);
        }
        
        List<String> modelMentionedUsers = Arrays.asList("talor");
        
        assertEquals(modelMentionedUsers.size(), mentionedUsersLowercase.size());
        assertTrue(modelMentionedUsers.containsAll(mentionedUsersLowercase));
    }
    
    @Test
    public void testGetMentionedUsersInStarAnEnd() {
        Set<String> mentionedUsers = Extract.getMentionedUsers(Arrays.asList(tweet3));
        List<String> mentionedUsersLowercase = new ArrayList<>();
        for (String user : mentionedUsers) {
            mentionedUsersLowercase.add(user);
        }
        
        List<String> modelMentionedUsers = Arrays.asList("glover", "greg");
        
        assertEquals(modelMentionedUsers.size(), mentionedUsersLowercase.size());
        assertTrue(modelMentionedUsers.containsAll(mentionedUsersLowercase));
    }
    
    @Test
    public void testGetMentionedUsersUsernameCharacterBeforeAt() {
        Set<String> mentionedUsers = Extract.getMentionedUsers(Arrays.asList(tweet6));
    
        assertTrue(mentionedUsers.isEmpty());
        
    }
    
    @Test
    public void testGetMentionedUsersEmptyAt() {
        Set<String> mentionedUsers = Extract.getMentionedUsers(Arrays.asList(tweet7));
        List<String> mentionedUsersLowercase = new ArrayList<>();
        for (String user : mentionedUsers) {
            mentionedUsersLowercase.add(user);
        }
        
        List<String> modelMentionedUsers = Arrays.asList("vezel");
        
        assertEquals(modelMentionedUsers.size(), mentionedUsersLowercase.size());
        assertTrue(modelMentionedUsers.containsAll(mentionedUsersLowercase));
        
    }

}
