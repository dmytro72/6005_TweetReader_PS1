package twitter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertArrayEquals;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

/*
 * Testing strategy for writenBy
 * Size of tweets : 0, 1, more then 1.
 * Case sensitivity 1: name is in upper case, lower case, different case.
 * Case sensitivity 2: authors in tweets are in  uppercase, lowercase, different case.
 * Tweets writen by : 0, all, some.
 * Check order tweets.
 * tweets not modified.
 * 
 * Testing strategy for inTimespan
 * Size of tweets : 0, 1, more then 1.
 * In this timespan : 0, all, some.
 * tweets not modified.
 * 
 * Testin strategy for contaning 
 * Size of tweets : 0, 1, more then 1.
 * Length of tweet text : 1 word, more then 1 words.
 * Place of word in text : at the beginning, at the end, at the middle
 * Case sensitivity in text : word is in upper case, lower case, different case.
 * Case sensitivity in word : word is in upper case, lower case, different case.
 * Quantity of finds : no finds, one finds, more then 1 finds.
 * Quantity of words : words not find, find some words, find all words
 * Find in different tweets : no words in tweets : words in one tweet, in some tweets, in all tweets.
 * Tweets not modified.S
 * 
 */

public class FilterTest {
    
    private static Instant d1;
    private static Instant d2;
    private static Instant d3;
    private static Instant d4;
    private static Instant d5;
    private static Instant d6;
    
    private static Tweet tweet1;
    private static Tweet tweet2;
    private static Tweet tweet3;
    private static Tweet tweet4;
    private static Tweet tweet5;
    private static Tweet tweet6;
    
    private static Timespan interval;
    
    @BeforeClass
    public static void setUpBeforeClass() {
        d1 = Instant.parse("2015-02-18T10:00:00Z");
        d2 = Instant.parse("2015-02-18T11:00:00Z");
        d3 = Instant.parse("2015-02-18T12:00:00Z");
        d4 = Instant.parse("2015-02-18T13:00:00Z");
        d5 = Instant.parse("2015-02-18T08:00:00Z");
        d6 = Instant.parse("2015-02-18T09:00:00Z");
        
        
        tweet1 = new Tweet(0, "alyssa", "is it reasonable to talk about rivest so much?", d1);
        tweet2 = new Tweet(1, "bbitdiddle", "rivest tAlk in 30 minutes #hype", d2);
        tweet3 = new Tweet(2, "ALYSSA", "GRAY talk he was at home", d3);
        tweet4 = new Tweet(3, "AlYsSa", "he talk me about JaVa", d4);
        tweet5 = new Tweet(4, "AlYsSa", "I nees some help", d5);
        tweet6 = new Tweet(5, "AlYsSa", "rivest talk in 34 minutes #hype", d6);

        Instant testDateStart = Instant.parse("2015-02-18T09:00:00Z");
        Instant testDateEnd = Instant.parse("2015-02-18T12:00:00Z");
        interval = new Timespan(testDateStart, testDateEnd);
    }
    
    @Test
    public void testWrittenByEmptyTweets() {
        List<Tweet> writtenBy = Filter.writtenBy(new ArrayList<Tweet>(), "alyssa");
        
        assertTrue(writtenBy.isEmpty());
    }
    
    @Test
    public void testWrittenByMultipleTweetsEmptyResult() {
        List<Tweet> writtenBy = Filter.writtenBy(Arrays.asList(tweet1, tweet2), "greg");
        
        assertTrue(writtenBy.isEmpty());
    }
    
    @Test
    public void testWrittenByMultipleTweetsMultipleResult() {
        List<Tweet> writtenBy = Filter.writtenBy(Arrays.asList(tweet1, tweet2, tweet3, tweet4), "ALYSSA");
        Tweet[] modelResult = {tweet1, tweet3, tweet4};
        
        assertArrayEquals(modelResult, writtenBy.toArray());
    }
    
    @Test
    public void testWrittenByMultipleTweetsAllInResult() {
        List<Tweet> writtenBy = Filter.writtenBy(Arrays.asList(tweet3, tweet4, tweet1), "AlYsSa");
        Tweet[] modelResult = {tweet3, tweet4, tweet1};
        
        assertArrayEquals(modelResult, writtenBy.toArray());
    }
    
    @Test
    public void testWrittenByOneTweetOneResult() {
        List<Tweet> writtenBy = Filter.writtenBy(Arrays.asList(tweet1), "alyssa");
        
        assertEquals(1, writtenBy.size());
        assertTrue(writtenBy.contains(tweet1));
    }

    @Test
    public void testWrittenByMultipleTweetsSingleResult() {
        List<Tweet> writtenBy = Filter.writtenBy(Arrays.asList(tweet1, tweet2), "alyssa");
        
        assertFalse(writtenBy.isEmpty());
        assertEquals(1, writtenBy.size());
        assertTrue(writtenBy.contains(tweet1));
    }
    
    @Test
    public void testWrittenByNotModified() {
        List<Tweet> tweets = Arrays.asList(tweet2, tweet4, tweet1, tweet3);
        Tweet[] tweetsBefore = new Tweet[tweets.size()];
        tweetsBefore = tweets.toArray(tweetsBefore);
        Filter.writtenBy(tweets, "alyssa");
        Tweet[] tweetsAfter = new Tweet[tweets.size()];
        tweetsAfter = tweets.toArray(tweetsAfter);
        
        assertArrayEquals(tweetsBefore, tweetsAfter);
    }
    
    @Test
    public void testInTimespanEmptyTweets() {
        List<Tweet> inTimespan = Filter.inTimespan(new ArrayList<Tweet>(), interval);
        
        assertTrue(inTimespan.isEmpty());
    }
    
    @Test
    public void testInTimespanOneTweetOneResult() {
        List<Tweet> inTimespan = Filter.inTimespan(Arrays.asList(tweet1), interval);
        Tweet[] modelResult = {tweet1};
        
        assertArrayEquals(modelResult, inTimespan.toArray());
    }
    
    @Test
    public void testInTimespanMultipleTweetsEmptyResult() {
        List<Tweet> inTimespan = Filter.inTimespan(Arrays.asList(tweet4, tweet5), interval);
        
        assertTrue(inTimespan.isEmpty());
    }
    
    @Test
    public void testInTimespanMultipleTweetsSomeOutOfTimespan() {
        List<Tweet> inTimespan = Filter.inTimespan(Arrays.asList(tweet1, tweet4, tweet5), interval);
        Tweet[] modelResult = {tweet1};
        
        assertArrayEquals(modelResult, inTimespan.toArray());
    }
    
    @Test
    public void testInTimespanBoundaryConditions() {
        List<Tweet> inTimespan = Filter.inTimespan(Arrays.asList(tweet3, tweet6, tweet2, tweet4), interval);
        Tweet[] modelResult = {tweet3, tweet6, tweet2};
        
        assertArrayEquals(modelResult, inTimespan.toArray());
        
    }
    
    @Test
    public void testInTimespanMultipleTweetsMultipleResults() {
        
        List<Tweet> inTimespan = Filter.inTimespan(Arrays.asList(tweet1, tweet2), interval);
        
        assertFalse(inTimespan.isEmpty());
        assertTrue(inTimespan.containsAll(Arrays.asList(tweet1, tweet2)));
    }
    
    @Test
    public void testInTimespanNotModified() {
        List<Tweet> tweets = Arrays.asList(tweet2, tweet4, tweet1, tweet3, tweet6, tweet5);
        Tweet[] tweetsBefore = new Tweet[tweets.size()];
        tweetsBefore = tweets.toArray(tweetsBefore);
        Filter.inTimespan(tweets, interval);
        Tweet[] tweetsAfter = new Tweet[tweets.size()];
        tweetsAfter = tweets.toArray(tweetsAfter);
        
        assertArrayEquals(tweetsBefore, tweetsAfter);
    }
    
    @Test
    public void testContainingEmptyTweets() {
        List<Tweet> containing = Filter.containing(new ArrayList<>(), Arrays.asList("talk"));
        
        assertTrue(containing.isEmpty());
    }
    
    @Test
    public void testContainingOneTweetOneWordOneResult() {
        List<Tweet> containing = Filter.containing(Arrays.asList(tweet4), Arrays.asList("java"));
        Tweet[] modelResult = {tweet4};
        
        assertArrayEquals(modelResult, containing.toArray());
    }
    
    @Test
    public void testContainingMultipleTweetsMultipleWordsNoResult() {
        List<Tweet> containing = Filter.containing(Arrays.asList(tweet1, tweet2, tweet3, tweet4),
                Arrays.asList("Deth", "birhD"));
        
        assertTrue(containing.isEmpty());        
    }
    
    @Test
    public void testContainingMultipleTweetsMultipleWordsSomeResult() {
        List<Tweet> containing = Filter.containing(Arrays.asList(tweet3, tweet1, tweet5, tweet2),
                Arrays.asList("gray", "TALK"));
        Tweet[] modelResult = {tweet3, tweet1, tweet2};
        
        assertArrayEquals(modelResult, containing.toArray());        
    }
    
    @Test
    public void testContainingFirstWordInTweet() {
        List<Tweet> containing = Filter.containing(Arrays.asList(tweet3), Arrays.asList("gRaY"));
        Tweet[] modelResult = {tweet3};
        
        assertArrayEquals(modelResult, containing.toArray());
    }
    
    @Test
    public void testContainingMultipleTweetsMultipleWordsAllInResult() {
        List<Tweet> containing = Filter.containing(Arrays.asList(tweet1, tweet2), Arrays.asList("talk"));
        
        assertFalse(containing.isEmpty());
        assertTrue(containing.containsAll(Arrays.asList(tweet1, tweet2)));
    }
    
    @Test
    public void testContainingNotModified() {
        List<Tweet> tweets = Arrays.asList(tweet2, tweet4, tweet1, tweet3, tweet6, tweet5);
        Tweet[] tweetsBefore = new Tweet[tweets.size()];
        tweetsBefore = tweets.toArray(tweetsBefore);
        Filter.containing(tweets, Arrays.asList("Java", "gray"));
        Tweet[] tweetsAfter = new Tweet[tweets.size()];
        tweetsAfter = tweets.toArray(tweetsAfter);
        
        assertArrayEquals(tweetsBefore, tweetsAfter);        
    }

}