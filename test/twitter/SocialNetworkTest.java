package twitter;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

/*
 * Warning: all the tests you write here must be runnable against any
 * SocialNetwork class that follows the spec. It will be run against several
 * staff implementations of SocialNetwork, which will be done by overwriting
 * (temporarily) your version of SocialNetwork with the staff's version.
 * DO NOT strengthen the spec of SocialNetwork or its methods.
 * 
 * In particular, your test cases must not call helper methods of your own
 * that you have put in SocialNetwork, because that means you're testing a
 * stronger spec than SocialNetwork says. If you need such helper methods,
 * define them in a different class. If you only need them in this test
 * class, then keep them in this test class.
 */

/*
 * Testing strategy for guessFollowsGraph
 * Number of tweets : 0, 1, more then 1.
 * Number of tweets' authors : 1, more then 1.
 * Same author found in other tweets : yes, no
 * Number mentioned names in tweets : 0, 1, more then 1.
 * Number mentioned names in one tweet : 0, 1, more then 1.
 * Same mentioned name in one tweet : yes, no.
 * Same mentioned name in different tweets of this author : yes, no
 * Case insensitive for authors : lower case, upper case, different case.
 * Case insensitive for mentioned names : lower case, upper case, different case.
 * 
 * Testing strategy for influencers.
 * Number of not empty values in Graph : 0, 1, more then 1.
 * Number mentioned names in one set : 1, more then 1.
 * Sameness : all mentioned names are same, some same, all different
 * Case insensitive for authors : lower case, upper case, different case.
 * Case insensitive for mentioned names : lower case, upper case, different case.
 * 
 * 
 */
public class SocialNetworkTest {
    
    private static Instant d1;
    private static Instant d2;
    private static Instant d3;
    private static Instant d4;
    
    private static Tweet tweet1;
    private static Tweet tweet2;
    private static Tweet tweet3;
    private static Tweet tweet4;
    
    @BeforeClass
    public static void setUpBeforeClass() {
        d1 = Instant.parse("2015-02-18T10:00:00Z");
        d2 = Instant.parse("2015-02-18T11:00:00Z");
        d3 = Instant.parse("2015-02-18T12:00:00Z");
        d4 = Instant.parse("2015-02-18T13:00:00Z");
        
        
        tweet1 = new Tweet(0, "alyssa", "is it reasonable to talk about rivest so much?", d1);
        tweet2 = new Tweet(1, "glover", "@TALOR's my friend", d2);
        tweet3 = new Tweet(2, "ALYSSA", "I see @tAlOr and @talor looks at me @veller", d3);
        tweet4 = new Tweet(3, "AlYsSa", "he talk me about JaVa @Miro", d4);
    }

    @Test
    public void testGuessFollowsGraphEmpty() {
        Map<String, Set<String>> followsGraph = SocialNetwork.guessFollowsGraph(new ArrayList<Tweet>());
        
        assertTrue(followsGraph.isEmpty());
    }
    
    @Test
    public void testGuessFollowsGraphOneTweetNoMentionedName() {
        Map<String, Set<String>> followsGraph = SocialNetwork.guessFollowsGraph(Arrays.asList(tweet1));
        Map<String, Set<String>> simpleGraph = simplifyGraph(followsGraph);
        
        assertTrue(simpleGraph.isEmpty());
    }
    
    @Test
    public void testGuessFollowsGraphOneTweetOneMentionedName() {
        Map<String, Set<String>> followsGraph = SocialNetwork.guessFollowsGraph(Arrays.asList(tweet2));
        Map<String, Set<String>> simpleGraph =  simplifyGraph(followsGraph);    // map for check
        
        Map<String, Set<String>> modelGraph = new HashMap<>();
        modelGraph.put("glover", new HashSet<String>(Arrays.asList("talor")));  // model map
        
        assertFalse(simpleGraph.isEmpty());                                     // check
        for (String key : simpleGraph.keySet()) {
            assertEquals(modelGraph.size(), simpleGraph.size());
            assertTrue(modelGraph.get(key).containsAll(simpleGraph.get(key)));
        }        
    }
    
    @Test
    public void testGuessFollowsGraphOneTweetMultipleMentionedNameSomeEqual() {
        Map<String, Set<String>> followsGraph = SocialNetwork.guessFollowsGraph(Arrays.asList(tweet3));
        Map<String, Set<String>> simpleGraph =  simplifyGraph(followsGraph);              // map for check
        
        Map<String, Set<String>> modelGraph = new HashMap<>();
        modelGraph.put("alyssa", new HashSet<String>(Arrays.asList("talor", "veller")));  // model map
        
        assertFalse(simpleGraph.isEmpty());                                               // check
        for (String key : simpleGraph.keySet()) {
            assertEquals(modelGraph.size(), simpleGraph.size());
            assertTrue(modelGraph.get(key).containsAll(simpleGraph.get(key)));
        }        
    }
    
    @Test
    public void testGuessFollowsGraphMultipleTweetMultipleMentionedName() {
        Map<String, Set<String>> followsGraph = SocialNetwork.guessFollowsGraph(Arrays.asList(tweet1, tweet2, tweet3, tweet4));
        Map<String, Set<String>> simpleGraph =  simplifyGraph(followsGraph);    // map for check
        
        Map<String, Set<String>> modelGraph = new HashMap<>();                  // model map
        modelGraph.put("glover", new HashSet<String>(Arrays.asList("talor")));
        modelGraph.put("alyssa", new HashSet<String>(Arrays.asList("talor", "veller", "miro")));
        
        assertFalse(simpleGraph.isEmpty());                                     // check
        for (String key : simpleGraph.keySet()) {
            assertEquals(modelGraph.size(), simpleGraph.size());
            assertTrue(modelGraph.get(key).containsAll(simpleGraph.get(key)));
        }        
    }
    
    @Test
    public void testGuessFollowsGraphNotModified() {
        List<Tweet> tweets = Arrays.asList(tweet2, tweet4, tweet1, tweet3);
        Tweet[] tweetsBefore = new Tweet[tweets.size()];
        tweetsBefore = tweets.toArray(tweetsBefore);
        SocialNetwork.guessFollowsGraph(tweets);
        Tweet[] tweetsAfter = new Tweet[tweets.size()];
        tweetsAfter = tweets.toArray(tweetsAfter);
        
        assertArrayEquals(tweetsBefore, tweetsAfter);
    }
    
    @Test
    public void testInfluencersEmpty() {
        Map<String, Set<String>> followsGraph = SocialNetwork.guessFollowsGraph(new ArrayList<Tweet>());
        List<String> influencers = SocialNetwork.influencers(followsGraph);
        
        assertTrue(influencers.isEmpty());
        
    }
    
    @Test
    public void testInfluencersOneValueMultipleMentionedNames() {
        Map<String, Set<String>> followsGraph = new HashMap<>();     // create test followsGraph
        followsGraph.put("alyssa", new HashSet<String>(Arrays.asList("Greg", "PiTeR","BRED")));
        
        Map<String, Integer> modelInfluence = AssertsAndHelpers.createMap(Arrays.asList("alyssa", "greg", "piter","bred"), // create model data
                Arrays.asList(0, 1, 1, 1));
        
        List<String> influencers = SocialNetwork.influencers(followsGraph);
        
        assertEquals(4, influencers.size());
        assertTrue(isInfluencerInOrder(influencers, modelInfluence));
    }
    
    @Test
    public void testInfluencersMultipleValueMentionedNamesAllSame() {
        Map<String, Set<String>> followsGraph = AssertsAndHelpers.createMap(Arrays.asList("Denny", "mysery", "welthy"),
                Arrays.asList(new HashSet<String>(Arrays.asList("alyssa")), new HashSet<String>(Arrays.asList("alyssa")),
                        new HashSet<String>(Arrays.asList("alyssa"))));         // create test followsGraph
        
        Map<String, Integer> modelInfluence = AssertsAndHelpers.createMap(Arrays.asList("denny", "mysery", "welthy", "alyssa"),
                Arrays.asList(0, 0, 0, 3));                                        // create model data
        
        List<String> influencers = SocialNetwork.influencers(followsGraph);
        
        assertEquals(4, influencers.size());
        assertTrue(isInfluencerInOrder(influencers, modelInfluence));
    }
    
    @Test
    public void testInfluencersMultipleValueMentionedNamesSomeSame() {
        Map<String, Set<String>> followsGraph = AssertsAndHelpers.createMap(Arrays.asList("Denny", "mysery", "welthy"),
                Arrays.asList(new HashSet<String>(Arrays.asList("alyssa")), new HashSet<String>(Arrays.asList("alyssa", "Greg", "welthy")),
                        new HashSet<String>(Arrays.asList("linda", "DARIA"))));         // create test followsGraph
        
        Map<String, Integer> modelInfluence = AssertsAndHelpers.createMap(
                Arrays.asList("denny", "mysery", "welthy", "alyssa", "greg", "linda", "daria"),
                Arrays.asList(0, 0, 1, 2, 1, 1, 1));                                        // create model data
        
        List<String> influencers = SocialNetwork.influencers(followsGraph);
        
        assertEquals(7, influencers.size());
        assertTrue(isInfluencerInOrder(influencers, modelInfluence));
    }
    
    @Test
    public void testInfluencersMultipleValueMentionedNamesAllDifferent() {
        Map<String, Set<String>> followsGraph = AssertsAndHelpers.createMap(Arrays.asList("Denny", "mysery", "welthy"),
                Arrays.asList(new HashSet<String>(Arrays.asList("alyssa")), new HashSet<String>(Arrays.asList("Greg")),
                        new HashSet<String>(Arrays.asList("linda", "DARIA"))));         // create test followsGraph
        
        Map<String, Integer> modelInfluence = AssertsAndHelpers.createMap(
                Arrays.asList("denny", "mysery", "welthy", "alyssa", "greg", "linda", "daria"),
                Arrays.asList(0, 0, 0, 1, 1, 1, 1));                                        // create model data
        
        List<String> influencers = SocialNetwork.influencers(followsGraph);
        
        assertEquals(7, influencers.size());
        assertTrue(isInfluencerInOrder(influencers, modelInfluence));
    }
    
    
    /* Helper methods */
    
    /** Reduce origin graph.
     * 
     * @param followsGraph graph of the social network.
     * @return simplify graph, which mentioned name in Set are lower case and delete key with empty Set value.
     */
    private static Map<String, Set<String>> simplifyGraph(final Map<String, Set<String>> followsGraph) {
        Map<String, Set<String>> simplifyGraph = new HashMap<>();
        for (String key : followsGraph.keySet()) {
            if (!(followsGraph.get(key).isEmpty())) {
                Set<String> names = new HashSet<>();
                for (String name : followsGraph.get(key)) {
                    names.add(name.toLowerCase());
                }
                simplifyGraph.put(key.toLowerCase(), names);
            }
        }
        return simplifyGraph;
    }
    
    /** Check are names in order with there influence
     * 
     * @param names list of username which are checked 
     * @param modelInfluence map of username with there influence
     * @return true if names in right order
     */
    private static boolean isInfluencerInOrder(final List<String> names, final Map<String, Integer> modelInfluence) {
        int previousInfluenceValue = Integer.MAX_VALUE;
        for (String name : names) {
            String nameInLowerCase = name.toLowerCase();
            if (modelInfluence.get(nameInLowerCase) > previousInfluenceValue) {return false;}
            previousInfluenceValue = modelInfluence.get(nameInLowerCase);
        }
        return true;
    }

}
