package twitter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/** Assert and helper methods
 * 
 * @author Dmytro Shaban
 *
 */
class AssertsAndHelpers {

    /* Asserts */
    
    /** Detect whether all tweets in list have distinct ids
     * 
     * @param tweets
     *            list of tweets, not modified by this method.
     * @return true if all tweets have distinct ids.
     */
    static boolean isDistinctTweets(final List<Tweet> tweets) {
        // extract ids, sort them, then find equal ids
        if (tweets.size() == 0) {return true;}      // empty list
        else {
            List<Long> ids = new ArrayList<>();
            for (Tweet tweet : tweets) {            // extract ids
                ids.add(tweet.getId());
            }
            Collections.sort(ids);                   // sort ids
            Long checkedId = ids.get(0);
            for (Long id : ids.subList(1, ids.size())) {    // find equal ids
                if (checkedId == id) {return false;}
                checkedId = id;
            }
            return true;
        }
    }

    /** Check username is valid
     * 
     * @param username tweet username for check
     * @return true if username is valid
     */
    static boolean isValidUsername(final String username) {
        
        for (int i = 0; i != username.length(); i++) {
            if (!(AssertsAndHelpers.isUsernameCharacter(username.charAt(i)))) {
                return false;
            }
        }
        return true;        
    }

    /** Detect is character valid username character.
     * 
     * @param ch character for check.
     * @return true if character is username character.
     */
    static boolean isUsernameCharacter(char ch) {
        return Character.isLetter(ch) ||
                Character.isDigit(ch) ||
                ch == '_' || ch == '-';
    }
    
    /** Check list of words for validity.
     * 
     * @param words List of words for check
     * @return true if all words are valid
     */
    static boolean isValidWords(final List<String> words) {
        for (String word : words) {
            if (!isValidWord(word)) {return false;}
        }
        return true;
    }

    /** Check word validity
     * 
     * @param word is sequence of characters
     * @return true if word is a nonempty sequence of nonspace characters.
     */
    static boolean isValidWord(final String word) {
        if (word.isEmpty()) {return false;}
        final int length = word.length();
        for (int i = 0; i != length; i++) {
            if (word.charAt(i) == ' ') {return false;}
        }
        return true;
    }

    /** Create new Map from two Lists
     * 
     * @param keys list of keys, elements must be hashable and all different.
     * @param values list of values, length of keys must be equal length of values.
     * @return new Map created from keys and values in pairs.
     */
    static <K, V>  Map<K, V> createMap(List<K> keys, List<V> values) {
        
        assert(keys.size() == new HashSet<K>(keys).size()) : "Elements in list of keys not different";
        assert(keys.size() == values.size()) : "Lists have a different length";
        
        Map<K, V> resultMap = new HashMap<>();
        for (int i = 0; i != keys.size(); i++) {
            resultMap.put(keys.get(i), values.get(i));
        }
        return resultMap;
    }
    
    /* Helper */
    

}
