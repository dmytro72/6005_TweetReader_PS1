package twitter;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Extract consists of methods that extract information from a list of tweets.
 * 
 * DO NOT change the method signatures and specifications of these methods, but
 * you should implement their method bodies, and you may add new public or
 * private methods or classes if you like.
 */
public class Extract {

    /**
     * Get the time period spanned by tweets.
     * 
     * @param tweets
     *            list of tweets with distinct ids, not modified by this method.
     * @return a minimum-length time interval that contains the timestamp of
     *         every tweet in the list.
     */
    public static Timespan getTimespan(List<Tweet> tweets) {
        
        assert (AssertsAndHelpers.isDistinctTweets(tweets)) : "Some tweets have equal id";
        
        Instant startTime = Instant.MAX;
        Instant endTime = Instant.MIN;
        if (tweets.size() == 0) {       // empty list
            startTime = Instant.MIN;
        }
        else {
            for (Tweet tweet : tweets) {
                Instant instant = tweet.getTimestamp();
                if (startTime.isAfter(instant)) {startTime = instant;}  // find min
                if (endTime.isBefore(instant)) {endTime = instant;}     // find max               
            }
        }
        return new Timespan(startTime, endTime);
    }

    /**
     * Get usernames mentioned in a list of tweets.
     * 
     * @param tweets
     *            list of tweets with distinct ids, not modified by this method.
     * @return the set of usernames who are mentioned in the text of the tweets.
     *         A username-mention is "@" followed by a Twitter username (as
     *         defined by Tweet.getAuthor()'s spec).
     *         The username-mention cannot be immediately preceded or followed by any
     *         character valid in a Twitter username.
     *         For this reason, an email address like bitdiddle@mit.edu does NOT 
     *         contain a mention of the username mit.
     *         Twitter usernames are case-insensitive, and the returned set may
     *         include a username at most once.
     */
    public static Set<String> getMentionedUsers(List<Tweet> tweets) {
        
        assert (AssertsAndHelpers.isDistinctTweets(tweets)) : "Some tweets have equal id";        
        
        Set<String> mentionedUsers = new HashSet<>();
        for (Tweet tweet : tweets) {
            String text = tweet.getText();
            boolean prefix = true;          //not userName symbol before current position
            boolean thisIsName = false;     // @ in current position and prefix is true
            for (int i = 0; i != text.length(); i++) {
                if (AssertsAndHelpers.isUsernameCharacter(text.charAt(i))) {
                    if (thisIsName) {
                        int endName = Extract.getIndexOfEndName(text, i);
                        mentionedUsers.add(text.substring(i, endName).toLowerCase());
                        thisIsName = false;
                        i = endName - 1;
                    }
                    prefix = false;
                }
                else if (text.charAt(i) == '@') {
                    if (prefix) {
                        thisIsName = true;
                    }
                    else {
                        prefix = true;
                    }
                }
                else {
                    prefix = true;
                    thisIsName = false;
                }
            }
        }
        return mentionedUsers;
    }
    
    /** Return index end of substring containing valid username (exclusive) in text
     * 
     * @param text string containing username
     * @param startSubstring index start of username (inclusive)
     * @return index end of username (exclusive)
     */
    private static int getIndexOfEndName(final String text, final int startSubstring) {
        for (int i = startSubstring + 1; i != text.length(); i++) {
            if (!(AssertsAndHelpers.isUsernameCharacter(text.charAt(i)))) {
                return i;
            }
        }
        return text.length();
    }
}
