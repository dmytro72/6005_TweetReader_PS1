package twitter;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Filter consists of methods that filter a list of tweets for those matching a
 * condition.
 * 
 * DO NOT change the method signatures and specifications of these methods, but
 * you should implement their method bodies, and you may add new public or
 * private methods or classes if you like.
 */
public class Filter {

    /**
     * Find tweets written by a particular user.
     * 
     * @param tweets
     *            a list of tweets with distinct ids, not modified by this method.
     * @param username
     *            Twitter username, required to be a valid Twitter username as
     *            defined by Tweet.getAuthor()'s spec.
     * @return all and only the tweets in the list whose author is username,
     *         in the same order as in the input list.
     */
    public static List<Tweet> writtenBy(List<Tweet> tweets, String username) {
        
        assert (AssertsAndHelpers.isDistinctTweets(tweets)) : "Some tweets have equal id";
        assert (AssertsAndHelpers.isValidUsername(username)) : "Username is not valid";
        
        final String lowercaseUsername = username.toLowerCase();
        final List<Tweet> tweetsWrittenBy = new ArrayList<>();
        for (Tweet tweet : tweets) {
            if (tweet.getAuthor().toLowerCase().equals(lowercaseUsername)) {
                tweetsWrittenBy.add(tweet);
            }
        }
        return tweetsWrittenBy;
    }

    /**
     * Find tweets that were sent during a particular timespan.
     * 
     * @param tweets
     *            a list of tweets with distinct ids, not modified by this method.
     * @param timespan
     *            timespan
     * @return all and only the tweets in the list that were sent during the timespan,
     *         in the same order as in the input list.
     */
    public static List<Tweet> inTimespan(List<Tweet> tweets, Timespan timespan) {
        
        assert (AssertsAndHelpers.isDistinctTweets(tweets)) : "Some tweets have equal id";
        
        final List<Tweet> tweetsInTimespan = new ArrayList<>();
        for (Tweet tweet : tweets) {
            Instant tweetTime = tweet.getTimestamp();
            Instant startTime = timespan.getStart();
            Instant endTime = timespan.getEnd();
            if ((tweetTime.isAfter(startTime) || tweetTime.equals(startTime)) &&
                (tweetTime.isBefore(endTime) || tweetTime.equals(endTime))) {
                
                tweetsInTimespan.add(tweet);
            }
        }
        return tweetsInTimespan;
    }

    /**
     * Find tweets that contain certain words.
     * 
     * @param tweets
     *            a list of tweets with distinct ids, not modified by this method.
     * @param words
     *            a list of words to search for in the tweets. 
     *            A word is a nonempty sequence of nonspace characters.
     * @return all and only the tweets in the list such that the tweet text (when 
     *         represented as a sequence of nonempty words bounded by space characters 
     *         and the ends of the string) includes *at least one* of the words 
     *         found in the words list. Word comparison is not case-sensitive,
     *         so "Obama" is the same as "obama".  The returned tweets are in the
     *         same order as in the input list.
     */
    public static List<Tweet> containing(List<Tweet> tweets, List<String> words) {
        
        assert (AssertsAndHelpers.isDistinctTweets(tweets)) : "Some tweets have equal id";  // Check tweets
        assert (AssertsAndHelpers.isValidWords(words)) : "Word list not valid";               // Check words
        
        List<Tweet> containing = new ArrayList<>();
        for (Tweet tweet : tweets) {
            for (String word : words) {
                if (Filter.isWordInString(word, tweet.getText())) {
                    containing.add(tweet);
                    break;
                }
            }
        }
        return containing;
    }
    
    /** Check if word is in text
     * 
     * @param modelWord not empty word which looks at text
     * @param text some words bounded by space
     * @return true if word is in text. Case insensitivity.
     */
    private static boolean isWordInString(final String modelWord, final String text) {
        
        assert(!(modelWord.isEmpty())) : "Looking word is empty.";
            
        final String[] words = text.split(" ");
        for (String word : words) {
            if (word.toLowerCase().equals(modelWord.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

}
